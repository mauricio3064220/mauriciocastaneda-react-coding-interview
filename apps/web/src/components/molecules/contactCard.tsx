import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import { InputInline } from './inputInline';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
  onChangeName: (id: string, name: string) => void;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { id, name, email },
  sx,
  onChangeName,
}) => {
  const handleChangeValue = (name: string) => {
    onChangeName(id, name);
  };
  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          {/* <Typography variant="subtitle1" lineHeight="1rem">
            {name}
          </Typography> */}
          <InputInline value={name} onChangeValue={handleChangeValue} />
          <Typography variant="caption" color="text.secondary">
            {email}
          </Typography>
        </Box>
      </Box>
    </Card>
  );
};
