import { Input, Typography } from '@mui/material';
import { useState } from 'react';

type Props = {
  value: string;
  onChangeValue: (value: string) => void;
};

export function InputInline({ value, onChangeValue }: Props) {
  const [currentValue, setCurrentValue] = useState<string>(value);
  const [isEditing, setIsEditing] = useState(false);

  const toggleEditing = () => {
    setIsEditing((prev) => !prev);
  };

  if (!isEditing) {
    return (
      <div onClick={toggleEditing}>
        <Typography variant="subtitle1" lineHeight="1rem">
          {value}
        </Typography>
      </div>
    );
  }

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCurrentValue(e.target.value);
  };

  const handleOk = () => {
    onChangeValue(currentValue);
    toggleEditing();
  };

  const handleCancel = () => {
    setCurrentValue(value);
    toggleEditing();
  };

  return (
    <div>
      <Input value={currentValue} onChange={handleChange} />
      <button onClick={handleOk}>Ok</button>
      <button onClick={handleCancel}>Cancel</button>
    </div>
  );
}
